import { useEffect, useState } from 'react'
import './App.css'
import ProductCard from './components/ProductCard/ProductCard'
import { useSelector } from 'react-redux'
import Cart from './components/Cart/Cart'
import CartButton from './components/CartButton/CartButton'

function App() {

  const [products, setProducts] = useState([
    {
      _id: 1,
      image: '/products/sundress-1.webp',
      category: 'Clothing',
      title: 'Casual Blue Sundress',
      price: 24.99,
    },
    {
      _id: 2,
      image: '/products/kurti-1.webp',
      category: 'Clothing',
      title: 'Midnight Charm Black Kurti',
      price: 39.99,
    },
    {
      _id: 3,
      image: '/products/croptop-1.webp',
      category: 'Clothing',
      title: 'Casual Blue Sundress',
      price: 24.99,
    },
    {
      _id: 4,
      image: '/products/shirt.webp',
      category: 'Clothing',
      title: 'Bold Red Checkered Shirt',
      price: 29.99,
    },
  ])

  const itemsInCart = useSelector(state => state.cart.items)
  const totalItems = itemsInCart.reduce((count, item) => {
    return count = count + item.quantity
  }, 0)

  return (
    <>
      <header className='h-20 py-4 bg-violet-950'>
        <div className='container mx-auto px-4 h-full flex flex-row items-center justify-between'>
          <div className='h-full flex flex-row items-center gap-2 text-lg'>
            <img className='w-8' src="/logos/logo.png" alt="" />
            <span className='text-green-400 font-bold'>GreenKart</span>
          </div>
          <a href="#">
            <div className='relative'>
              <span class="material-symbols-outlined text-slate-300">
                local_mall
              </span>
              <span className='absolute top-0 right-0 translate-x-1/3 -translate-y-1/3 rounded-full w-4 h-4 bg-pink-400 font-bold text-xs flex flex-row items-center justify-center '>{totalItems}</span>
            </div>
          </a>
        </div>
      </header>
      <main>
        <section className='py-16'>
          <div className='container mx-auto px-4'>
            <h2 className='text-xl font-bold'>Popular products</h2>
            <div className='grid grid-cols-3 gap-2 mt-8'>
              {
                products.map(product => {
                  return (
                    <ProductCard key={product._id} product={product} />
                  )
                })
              }
            </div>
          </div>
        </section>
        <div className='container mx-auto px-4'>
          <Cart />
          <div className='bg-slate-200 p-6'>
            <h3 className='text-lg font-bold'>Puchase details</h3>
            <table className='mt-6 w-full table-auto border-collapse border border-slate-500'>
              <thead>
                <tr className=''>
                  <th className='border border-slate-600'>sl.no</th>
                  <th className='border border-slate-600'>Item</th>
                  <th className='border border-slate-600'>MRP</th>
                  <th className='border border-slate-600'>Quantity</th>
                  <th className='border border-slate-600'>Price</th>
                </tr>
              </thead>
              <tbody>
                {
                  itemsInCart.map((item, index) => {
                    return (
                      <tr key={item._id}>
                        <td className='border border-slate-700'>{index + 1}</td>
                        <td className='border border-slate-700'>{item.title}</td>
                        <td className='text-center border border-slate-700'>{item.price}</td>
                        <td className='text-center border border-slate-700'>{item.quantity}</td>
                        <td className='text-center border border-slate-700'>{item.price * item.quantity}</td>
                      </tr>
                    )
                  })
                }

              </tbody>

            </table>
            <CartButton />
          </div>
        </div>
      </main>
      <footer></footer>
    </>
  )
}

export default App
