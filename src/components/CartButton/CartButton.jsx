import React from 'react';
import { useSelector } from 'react-redux';

function CartButton(props) {
    const itemsInCart = useSelector(state => state.cart.items)
    const totalPrice = itemsInCart.reduce((total, item) => {
        return total + item.quantity * item.price
    }, 0)
    return (
        <button className='px-4 py-2 text-white rounded-md hover:bg-violet-900 bg-violet-950 w-full mt-6'>
            Checkout ${totalPrice}
        </button>
    );
}

export default CartButton;