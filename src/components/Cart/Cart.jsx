import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { decrementQuantity, incrementQuantity } from '../../features/cart/cartSlice';

function Cart(props) {

    const productsInCart = useSelector(state => state.cart.items)

    return (
        <section className='py-16'>
            <div className="container mx-auto px-4">
                <h2 className='text-xl font-bold'>Cart</h2>
                <div className='mt-8 flex flex-col gap-4'>
                    {
                        productsInCart.map(product => {
                            const dispatch = useDispatch()
                            return (
                                <article key={product._id} className='gap-4 shadow-md border p-4 rounded-sm flex flex-row items-center'>
                                    <img className='w-24 h-24 object-cover object-center' src={product.image} />
                                    <div>
                                        <h3 className='font-bold'>{product.title} </h3>
                                        <span className='text-gray-600 text-sm'>${product.price}</span>
                                        <div className='mt-4 flex flex-row items-center'>
                                            <button onClick={() => dispatch(decrementQuantity(product._id))} className='w-6 h-6 bg-gray-300 flex flex-row items-center justify-center'>-</button>
                                            <span className=' w-6 h-6 text-sm flex flex-row items-center justify-center'>{product.quantity}</span>
                                            <button onClick={() => dispatch(incrementQuantity(product._id))} className='w-6 h-6 bg-gray-300 flex flex-row items-center justify-center'>+</button>
                                        </div>
                                    </div>
                                </article>
                            )
                        })
                    }

                </div>
            </div>
        </section>
    );
}

export default Cart;